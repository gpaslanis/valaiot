/*forward declared structs in gpiod.h are defined in core.h
REFER:https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/tree/lib/core.c
struct gpiod_chip;
struct gpiod_line;
REFER:https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/tree/lib/iter.c?id=f5f4794647e1d99d92af625f89951e638995577c
struct gpiod_chip_iter;
struct gpiod_line_iter;
struct gpiod_line_bulk;
*/

#include <stdbool.h>

struct line_fd_handle {
	int fd;
	int refcount;
};

struct gpiod_line {
	unsigned int offset;
	int direction;
	int active_state;
	bool used;
	bool open_source;
	bool open_drain;

	int state;
	bool up_to_date;

	struct gpiod_chip *chip;
	struct line_fd_handle *fd_handle;

	char name[32];
	char consumer[32];
};

struct gpiod_chip {
	struct gpiod_line **lines;
	unsigned int num_lines;

	int fd;

	char name[32];
	char label[32];
};

struct gpiod_chip_iter {
	struct gpiod_chip **chips;
	unsigned int num_chips;
	unsigned int offset;
};

struct gpiod_line_iter {
	struct gpiod_line **lines;
	unsigned int num_lines;
	unsigned int offset;
};
<!-- [![Linux & Mac build status](https://img.shields.io/travis/Dart-Code/Dart-Code/master.svg?label=mac+%26+linux)](https://travis-ci.org/Dart-Code/Dart-Code)
[![Windows build status](https://img.shields.io/appveyor/ci/DanTup/Dart-Code/master.svg?label=windows&logoWidth=-1)](https://ci.appveyor.com/project/DanTup/dart-code) [![Gitter Chat](https://img.shields.io/badge/chat-online-blue.svg)](https://gitter.im/dart-code/Dart-Code) [![Follow on Twitter](https://img.shields.io/badge/twitter-dartcode-blue.svg)](https://twitter.com/@osstekz) [![Contribute to Dart Code](https://img.shields.io/badge/help-contribute-551A8B.svg)](https://github.com/Dart-Code/Dart-Code/blob/master/CONTRIBUTING.md) -->

[![Contribute to valaIOT](https://img.shields.io/badge/help-donate-551A8B.svg)](https://www.paypal.me/GeorgeAslanis)
[![Contribute to valaIOT](https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_100x26.png)](https://www.paypal.me/GeorgeAslanis)

# valaIOT/vapis
Inspired by AlThomas' answer to [![stackoverflow/56861493]](https://stackoverflow.com/questions/56861493/create-binding-for-define-pointer-address)

Also working on **ch2vapi** (a 'C header' parser) to help with generating the vapis. If you've found an issue or have a suggestion, [open an issue](https://gitlab.com/gpaslanis/valaIOT/issues/new).

# How I compile test programs
  * valac -X -I**Projects**/valaIOT/vapis/src -X -w --save-temps --debug --enable-checking --enable-experimental --disable-warnings --vapidir=**Projects**/valaIOT/vapis/src --pkg=libgpiod --pkg=gpiod-x --pkg=posix --output=**Projects**/valaIOT/bin/ut_libgpiod.exe **Projects**/valaIOT/vapis/test/ut_libgpiod.vala
  * valac -X -I**Projects**/valaIOT/vapis/src -X -w --save-temps --debug --enable-checking --enable-experimental --disable-warnings --vapidir=**Projects**/valaIOT/vapis/src --pkg=linux_gpio --pkg=posix --output=**Projects**/valaIOT/bin/ut_linux_gpio.exe **Projects**/valaIOT/vapis/test/ut_linux_gpio.vala

# Contribute your knowledge and experience:
- Update the [wiki](https://gitlab.com/gpaslanis/valaiot/wikis/home) documentation section
- Request potential **ch2vapi** changes especially in CCode generation/requirements
- Submit test code updates/changes.

# Vala Language useful links
- **AlThomas' ManualBindings**: https://wiki.gnome.org/Projects/Vala/ManualBindings
- **Vala's Official Page**: https://wiki.gnome.org/Projects/Vala
- **Valadoc**: http://www.valadoc.org/

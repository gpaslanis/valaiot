/*~ Formatted by vlangsrvc.exe*/
using Libgpiod;

//REFER:https://stackoverflow.com/questions/56861493/create-binding-for-define-pointer-address
public struct Timespec: Posix.timespec {}
class UT_Libgpiod {
	private int n_tests = 0;
	private int n_passed = 0;
	private void test_bits() {
		n_tests++;
		int ishift = 3;
		ulong uval = gpiod_bit(ishift);
		bool bResult =(uval == 8);
		stdout.printf("...gpiod_bit(%d)==%lu [%s]\n", ishift, uval, bResult.to_string());
		if (bResult)
			n_passed++;
		}
	private void test_values() {
		try {
			n_tests++;
			stdout.puts("Libgpiod:test_values\r\n");
			stdout.printf("...gpiod_version:%s\n", gpiod_version_string());
			test_bits();
			n_passed++;
			}
		catch(Error e) {
			error("Libgpiod:test_values err:%s", e.message);
			}
		}
	public int run_all() {
		debug("UT_Libgpiod:run_all");
		test_values();
		return run_check();
		}
	private int run_check() {
		debug("UT_Libgpiod:run_check");
		if (n_passed != n_tests) {
			warning("Failed %d/%d tests", n_tests - n_passed, n_tests);
			return 1;
			}
		debug("Passed all %d tests", n_tests);
		return 0;
		}
	}
public static int main(string[] args) {
	UT_Libgpiod ut = new UT_Libgpiod();
	return ut.run_all();
	}

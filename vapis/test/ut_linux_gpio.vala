/*~ Formatted by vlangsrvc.exe*/
using LinuxGpio;

//REFER:https://stackoverflow.com/questions/56861493/create-binding-for-define-pointer-address
//REFER:https://stackoverflow.com/questions/41396837/virtual-gpio-emulation
//REFER:https://github.com/torvalds/linux/blob/master/drivers/gpio/gpio-mockup.c
//REFER:https://github.com/omnirom/android_kernel_asus_sm8150/blob/c829782bb7a9ff83b3991b1e201192b435983dae/tools/testing/selftests/gpio/gpio-mockup-chardev.c
internal errordomain AppErrors {
	OK,
	ERROR}
class UT_LinuxGpio {
	private int n_tests = 0;
	private int n_passed = 0;
	private void check_gpiochip(uint chip_number) {
		int fd = -1;
		n_tests++;
		try {
			string chip_name =("/dev/gpiochip%u".printf(chip_number));
			stdout.printf("LinuxGpio:check %s\r\n", chip_name);
			//Vala structs must be initialized before first use. A Vala struct cannot implement interfaces.
			GpiochipInfo chip_info = GpiochipInfo();
			fd = Posix.open(chip_name, Posix.O_RDWR);
			if (fd == -1) {
				throw new AppErrors.ERROR("open:%s err:%s\n", chip_name, strerror(errno));
				}
			int ret = Posix.ioctl(fd, GPIO_GET_CHIPINFO_IOCTL, & chip_info);
			if (ret == -1) {
				throw new AppErrors.ERROR("ioctl:%s:GPIO_GET_CHIPINFO_IOCTL err:%s\n", chip_name, strerror(errno));
				}
			stdout.printf("...gpio_get_chipinfo_ioctl: name[%s] label[%s] lines[%u]\n",(string)
				chip_info.name,(string)
				chip_info.label, chip_info.lines);
			n_passed++;
			}
		catch(Error e) {
			error("check_gpiochip: err:%s", e.message);
			}
		finally {
			if (fd >= 0)
				Posix.close(fd);
			}
		}
	private void check_constants() {
		n_tests++;
		stdout.puts("LinuxGpio:check_constants\r\n");
		stdout.printf("...gpio_kernel:%lu\n", eGpiolineFlag.KERNEL);
		stdout.printf("...gpio_handle_get_line_values_ioctl:%ld\n", GPIOHANDLE_GET_LINE_VALUES_IOCTL);
		stdout.printf("...gpio_handle_set_line_values_ioctl:%ld\n", GPIOHANDLE_SET_LINE_VALUES_IOCTL);
		stdout.printf("...gpio_event_request_both_edges:%lu\n", eGpioeventRequest.BOTH_EDGES);
		stdout.printf("...gpio_event_rising_edge:%d\n", eGpioeventRequest.RISING_EDGE);
		stdout.printf("...gpio_event_falling_edge:%d\n", eGpioeventRequest.FALLING_EDGE);
		stdout.printf("...gpio_get_chipinfo_ioctl:%ld\n", GPIO_GET_CHIPINFO_IOCTL);
		stdout.printf("...gpio_get_lineinfo_ioctl:%ld\n", GPIO_GET_LINEINFO_IOCTL);
		stdout.printf("...gpio_get_linehandle_ioctl:%ld\n", GPIO_GET_LINEHANDLE_IOCTL);
		stdout.printf("...gpio_get_lineevent_ioctl:%ld\n", GPIO_GET_LINEEVENT_IOCTL);
		n_passed++;
		}
	public int run_all() {
		debug("UT_LinuxGpio:run_all");
		check_constants();
		check_gpiochip(0);
		return run_check();
		}
	private int run_check() {
		debug("UT_LinuxGpio:run_check");
		if (n_passed != n_tests) {
			warning("Failed %d/%d tests", n_tests - n_passed, n_tests);
			return 1;
			}
		debug("Passed all %d tests", n_tests);
		return 0;
		}
	}
public static int main(string[] args) {
	UT_LinuxGpio ut = new UT_LinuxGpio();
	return ut.run_all();
	}
